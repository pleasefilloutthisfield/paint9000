# Paint9000
A light and easy frontend image manipulation tool.
Chains smoothly to existing integration to reduce image sizes on uploads.  

## Fast demo
1) Embed the dist.js script to a site or console. 
2) Run the following: 
```javascript
new Paint9000 (false, {editor:true, downloadWhenDone:true});
```
You should see an input field for uploading an image at the bottom of the page. 
From here you can upload an image or paint a picture. Play with the slider to adjust quality and hit download to save the current state. 

## Common use

Using Paint9000 as a "middleware" before the actual  upload to server function is called. 
```javascript
new Paint9000 (
    document.getElementById("myFileInput"),
    {   
        maxLength:1000,
        quality: 0.7
    },
    () => { 
        
        console.log("I have replaced the files in the <input>, with scaled and compressed versions");
        const newFiles = document.getElementById("myFileInput").files; 
        runUploadImageToServerFunction( newFiles );
    }
);
```
Using Paint9000 as a tool for batch processing some unnecessarily large images. 
Make sure you <input> has multiple attribute, then just drop batches of 10 images at time and it will download the conversions  instantly 
```javascript
new Paint9000 (
    document.getElementById("myFileInput"),
    {   
        downloadWhenDone:true
        maxLength:1920,
        quality: 0.8,

    }
);
```

## Docs

The class paint9000 takes in three arguments.
1) The file input.
2) The options.
3) The callback. 

Configuration
All options are listed as follows:

Options name : Default value | Type | "Description"

- downloadWhenDone: false | Boolean | "Determines if post processing should trigger a download."
- postFix : "" | String | "Puts a postfix on processed images"
- fileType : "jpeg" | String | "Desired filetype output, jpeg is preferred"
- quality : 0.9 | Number | "range from 0.1 - 1, determines compression level"
- maxLength : 1920 | Number | "The post processed max length of wither height or width" 
- editor: false | Boolean | "Enables an editor functionality, ideal for testing and determining quality"
- ignoredMimeTypes : [] | Array | "Any defined mimetypes will be completely ignored, for instance ignoredMimeTypes:["image/gif"] will prevent animated GIFS from being processed as single images" 

Enjoy.