"use strict"

export default class Paint9000 {
    constructor(fileInputElement, settings, cb) {
        settings = settings || {};
        const canvas = document.createElement("canvas");
        canvas.id = "paint9000Canvas";
        const tempCanvas = document.createElement("canvas");
        this.settings = {
            fileInput: fileInputElement ? fileInputElement : document.getElementById('paint9000Input'),
            canvas: canvas,
            CTX: canvas.getContext("2d"),
            tempCanvas: tempCanvas,
            tempCTX: tempCanvas.getContext("2d"),
            link: document.createElement('a'),
            paint9000: this,
            fileName: "Unamed",
            currentSize: 0,
            progress: 0,
            progressExpected: 0,
            batch: 0,
            expectedFilesCount: 1,
            painting: false,
            color: "#000000",
            paintSize: 5,
            lines: [],
        }
        this.settings.autoDownload = settings.downloadWhenDone ? true : false;
        this.settings.postFix = settings.postFix ? settings.postFix : "";
        this.settings.fileType = settings.fileType ? settings.fileType : "jpeg";
        this.settings.mimeType = "image/" + this.settings.fileType;
        this.settings.ignoredMimeTypes = settings.ignoredMimeTypes ? settings.ignoredMimeTypes : [];
        this.settings.quality = settings.quality && settings.quality < 1 ? settings.quality : 0.9;
        this.settings.editor = settings.editor ? true : false;
        this.settings.maxLength = settings.maxLength ? settings.maxLength : 1920;

        if (!this.settings.fileInput && !this.settings.editor) {
            console.error("No file input detected, either submit af fileinput to the new class instance or give the id 'paint9000Input', to an exisiting fileinput.");
            return;
        }
        if (this.settings.editor) {
            this.editor();
        }

        if (cb) {
            this.settings.cb = cb;
        }
        this.settings.fileInput.addEventListener("change", (files) => { this.imageUploadHandler(files) });
        window.paint9000Object = this.settings;
    }
    getProgress() {
        //Progress i determined by 3 steps per image: scale, compression and finalize multiplied with total image amount.
        return 100 / (this.settings.progressExpected * this.settings.expectedFilesCount) * (this.settings.progress * this.settings.expectedFilesCount);
    }
    getImageName(fileNameOnly, forceName) {
        const settings = this.settings;
        let name = forceName ? forceName : settings.fileName;
        if (fileNameOnly) { return name; }
        if (name.indexOf(".") > -1) {
            name = name.split(".")[0];
        }
        return (name + settings.postFix + "." + settings.fileType)
    }
    incrementProgress() {
        if (this.settings.progress < this.settings.progressExpected) {
            this.settings.progress++;
        }
    }
    update() {
        const settings = this.settings;
        if (settings.editor) {
            settings.editorString.innerHTML = this.getImageName() + " - Quality: " + settings.quality + " Size:" + settings.currentSize + " <br> Progress: " + this.getProgress();
        }
    }
    finalize() {
        this.settings.fileInput.files = this.settings.dataTransfer.files;
        this.incrementProgress();
        this.update();
        if (this.settings.autoDownload) {
            this.download();
        }
        this.settings.lastPerformanceMS = performance.now() - this.settings.performanceNow;
        if (this.settings.cb) {
            this.settings.cb(this.settings.fileInput, this.settings);
        }
        if (this.settings.ogEditor) {
            this.settings.editor = true;
        }
    }
    editor() {
        const paintClass = this;
        const settings = this.settings;
        const toolbar = document.createElement("div");
        toolbar.id = "paint9000Toolbar";
        const editorInput = (type, value) => {
            let elm = document.createElement("input");
            if (type) {
                elm.type = type;
            }
            if (value) {
                elm.value = value;
            }
            toolbar.appendChild(elm);
            return elm;
        }

        document.body.appendChild(toolbar);
        toolbar.appendChild(settings.canvas);

        if (!this.settings.fileInput) {
            const fileInput = editorInput("file")
            fileInput.accept = "image/*";
            fileInput.multiple = true;
            this.settings.fileInput = fileInput;
        }

        const colorPicker = editorInput("color", settings.color);
        colorPicker.addEventListener("change", (e) => { settings.color = e.target.value; });

        const sizePicker = editorInput("number", settings.paintSize, 5);
        sizePicker.addEventListener("change", (e) => { settings.paintSize = e.target.value; });

        const undoButton = document.createElement("button");
        undoButton.innerHTML = "Undo";
        undoButton.onclick = () => { paintClass.redraw(true) };
        toolbar.appendChild(undoButton);

        const button = document.createElement("button");
        button.innerHTML = "Download";
        button.onclick = () => {
            if (settings.fileInput.files[0] || settings.lines.length > 0) {
                this.redraw(false, true)
                this.download();
            }
        };
        toolbar.appendChild(button);

        const range = editorInput("range", (settings.quality * 100));
        range.addEventListener("change", (e) => {
            if (e.target.value) {
                settings.quality = (e.target.value - []) / 100;
                if (settings.fileInput.files[0]) {
                    paintClass.redraw(false, true);
                }

            }
        });

        const editorString = document.createElement("p");
        settings.editorString = editorString;
        toolbar.appendChild(editorString);

        settings.canvas.addEventListener('mousedown', (e) => { paintClass.mousedown(e); });
        settings.canvas.addEventListener('mousemove', (e) => { paintClass.mousemove(e); });
        settings.canvas.addEventListener('mouseup', () => { paintClass.settings.painting = false; paintClass.settings.lines.push({ x: 0, y: 0, size: 0, color: null }) });

        const style = document.createElement("style");
        style.innerHTML = "#paint9000Canvas { margin-right: 100%;}#paint9000Toolbar {display: flex;flex-wrap: wrap;align-content: center;align-items: center;} #paint9000Toolbar input, #paint9000Toolbar button { margin: 0 10px 0 0; max-width: 90px; cursor:pointer; }";
        document.body.appendChild(style);
    }

    getMousePos(e) {
        const canvas = this.settings.canvas;
        const rect = canvas.getBoundingClientRect();
        return {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top
        };
    }
    redraw(undo, finalize) {
        this.settings.progress = 0;
        const lines = this.settings.lines;
        const CTX = this.settings.tempCTX;
        if (undo) {
            lines.pop();
        }
        this.compressImage(finalize, () => {
            for (let i = 1; i < lines.length; i++) {
                const line = lines[i];
                const prevLine = lines[i - 1];

                if (line.size > 0 && (prevLine.x + prevLine.y > 0)) {
                    CTX.beginPath();
                    CTX.moveTo(prevLine.x, prevLine.y);
                    CTX.lineWidth = line.size;
                    CTX.lineCap = "round";
                    CTX.strokeStyle = line.color;
                    CTX.lineTo(line.x, line.y);
                    CTX.stroke();
                }
            }
        });
        this.incrementProgress();
    }
    mousedown(e) {
        const currentPosition = this.getMousePos(e);
        const CTX = this.settings.CTX;
        this.settings.painting = true;
        CTX.moveTo(currentPosition.x, currentPosition.y)
        CTX.beginPath();
        CTX.lineWidth = this.settings.paintSize;
        CTX.lineCap = "round";
        CTX.strokeStyle = this.settings.color;
    }
    mousemove(e) {
        if (this.settings.painting) {
            const CTX = this.settings.CTX;
            CTX.strokeStyle = this.settings.color;
            const currentPosition = this.getMousePos(e);
            CTX.lineTo(currentPosition.x, currentPosition.y)
            CTX.stroke();
            this.settings.lines.push({ x: currentPosition.x, y: currentPosition.y, size: this.settings.paintSize, color: this.settings.color });
        }
    }

    imageUploadHandler(files) {
        if (!files.target.files) { return }
        this.settings.performanceNow = performance.now();
        this.settings.dataTransfer = new DataTransfer();
        this.settings.lines = [];
        this.settings.batch = 0;
        this.settings.batchFiles = files.target.files;
        this.settings.expectedFilesCount = this.settings.batchFiles.length;
        this.settings.progressExpected = 3 * this.settings.expectedFilesCount;
        if (this.settings.editor && this.settings.expectedFilesCount > 1) {
            this.settings.editor = false;
            this.settings.ogEditor = true;
        }
        this.nextBatch();
    }
    nextBatch() {
        this.loadFileAsImage(this.settings.batchFiles[this.settings.batch]);
        this.settings.batch++;
    }
    loadFileAsImage(file) {
        if (!file) { return; }
        const paintClass = this;
        const settings = this.settings;
        const fileReader = new FileReader();
        this.settings.fileName = file.name;
        if (!file.type.startsWith('image') || settings.ignoredMimeTypes.includes(file.type)) {
            /* 
                Ignore this file, but do add it to our result,
                in case it is some wierd multi upload dropbox / google drive like sorta deal.
            */
            settings.dataTransfer.items.add(file);
            return;
        }
        fileReader.onloadend = function (reader) {
            const image = new Image();
            settings.originalImage = image;
            image.src = reader.target.result;
            image.onload = function () {
                paintClass.steppedScaling(image);
            }
        }
        fileReader.readAsDataURL(file);
    }
    steppedScaling(image) {
        this.incrementProgress();
        const CTX = this.settings.CTX;
        const canvas = this.settings.canvas;
        const tempCanvas = this.settings.tempCanvas
        const tempCTX = this.settings.tempCTX
        const step = 0.5
        const mul = 1 / step;

        const longestLengthName = image.height > image.width ? "height" : "width";
        let longestLength = image.height > image.width ? image.height : image.width;
        if (longestLength > this.settings.maxLenght) {
            longestLength = this.settings.maxLenght;
        }
        let currentDimension = {
            width: Math.floor(image.width * step),
            height: Math.floor(image.height * step)
        }
        this.settings.currentMaxLength = longestLengthName;

        if (longestLength <= this.settings.maxLength) {
            canvas.height = image.height;
            canvas.width = image.width;
            CTX.drawImage(image, 0, 0, image.width, image.height);
            return this.scaleCompleted();
        }
        else if (this.settings.maxLength < longestLength) {
            longestLength = this.settings.maxLength;
        }

        if (longestLengthName === "width") {
            canvas.width = longestLength;
            canvas.height = canvas.width * image.height / image.width;
        }
        else {
            canvas.height = longestLength;
            canvas.width = canvas.height * image.width / image.height;
        }

        if (image.width * step > longestLength) {
            tempCanvas.width = currentDimension.width;
            tempCanvas.height = currentDimension.height;
            tempCTX.drawImage(image, 0, 0, currentDimension.width, currentDimension.height);

            while (currentDimension.width * step > longestLength) {
                currentDimension = {
                    width: Math.floor(currentDimension.width * step),
                    height: Math.floor(currentDimension.height * step)
                };
                tempCTX.drawImage(tempCanvas, 0, 0, currentDimension.width * mul, currentDimension.height * mul, 0, 0, currentDimension.width, currentDimension.height);
            }

            CTX.drawImage(tempCanvas, 0, 0, currentDimension.width, currentDimension.height, 0, 0, canvas.width, canvas.height);
        }
        else {
            CTX.drawImage(image, 0, 0, canvas.width, canvas.height);
        }
        return this.scaleCompleted();
    }
    scaleCompleted() {
        this.settings.scaledImage = this.settings.canvas.toDataURL();
        return this.compressImage();
    }
    compressImage(finalize, middleware) {
        this.incrementProgress();
        const paintClass = this;
        const settings = this.settings;
        const CTX = this.settings.CTX;
        const canvas = this.settings.canvas;
        const tempCanvas = this.settings.tempCanvas;
        const tempCTX = this.settings.tempCTX
        const originalWidth = canvas.width;
        const originalHeight = canvas.height;
        tempCanvas.width = originalWidth;
        tempCanvas.height = originalHeight
        const imgToCompress = new Image();
        imgToCompress.src = this.settings.scaledImage;
        imgToCompress.onload = (() => {
            tempCTX.drawImage(
                imgToCompress,
                0,
                0,
                originalWidth,
                originalHeight
            );

            if (middleware) {
                middleware();
            }

            paintClass.update();

            // reducing the quality of the image
            tempCanvas.toBlob(
                (blob) => {
                    if (blob) {
                        const blobURL = URL.createObjectURL(blob);
                        const compressedImage = new Image();
                        compressedImage.src = blobURL;

                        compressedImage.onerror = () => {
                            URL.revokeObjectURL(compressedImage.src);
                            console.error("Cannot load compressed image");
                        };
                        compressedImage.onload = () => {
                            URL.revokeObjectURL(compressedImage.src);
                            CTX.drawImage(compressedImage, 0, 0, originalWidth, originalHeight);
                            settings.currentSize = paintClass.bytesToSize(blob.size);
                            const filesCount = settings.fileInput.files.length;
                            const compressedFile = new File([blob], paintClass.getImageName(true), {
                                type: blob.type,
                            });
                            settings.finalFile = compressedFile;
                            if (settings.editor) {
                                for (let i = 0; i < filesCount; i++) {
                                    let dataTransferFile = settings.dataTransfer.files[i];
                                    if (dataTransferFile && compressedFile.name === dataTransferFile.name) {
                                        settings.dataTransfer.items.remove(compressedFile);
                                    }
                                }
                            }
                            settings.dataTransfer.items.add(compressedFile);

                            if ((!settings.editor || finalize) && settings.dataTransfer.files.length >= settings.expectedFilesCount) {
                                paintClass.finalize(compressedFile);
                            }
                            else if (settings.editor) {
                                paintClass.incrementProgress();
                            }
                            else if (settings.batch < settings.expectedFilesCount) {
                                paintClass.nextBatch();
                            }
                            paintClass.update();


                        }
                    }
                },
                settings.mimeType,
                settings.quality
            );
        })
    }
    bytesToSize(bytes) {
        const sizes = ["Bytes", "KB", "MB", "GB", "TB"];
        if (bytes === 0) {
            return "0 Byte";
        }
        const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + " " + sizes[i];
    }
    download() {
        const settings = this.settings;
        const link = settings.link;
        for (let i = 0; i < settings.expectedFilesCount; i++) {
            const file = settings.fileInput.files[i];
            if (file || settings.expectedFilesCount === 1) {
                link.download = this.getImageName(false, file && file.name ? file.name : "aFileWithNoName");
                link.href = file ? URL.createObjectURL(file) : settings.canvas.toDataURL();
                link.click();
            }

        }

    }
}
